//
//  ViewController.swift
//  newInstant
//
//  Created by Guillermo Cádiz Pizarro on 24-06-15.
//  Copyright (c) 2015 Guillermo Cádiz Pizarro. All rights reserved.
//

import Cocoa
import AVFoundation
import AppKit

class ViewController: NSViewController {

    @IBOutlet weak var appBG: NSImageView!
    
    @IBOutlet weak var buttonQ: NSButton!
    @IBOutlet weak var buttonW: NSButton!
    @IBOutlet weak var buttonE: NSButton!
    @IBOutlet weak var buttonR: NSButton!
    @IBOutlet weak var buttonA: NSButton!
    @IBOutlet weak var buttonS: NSButton!
    @IBOutlet weak var buttonD: NSButton!
    @IBOutlet weak var buttonF: NSButton!
    @IBOutlet weak var buttonZ: NSButton!
    @IBOutlet weak var buttonX: NSButton!
    @IBOutlet weak var buttonC: NSButton!
    @IBOutlet weak var buttonV: NSButton!
    @IBOutlet weak var buttonSpace: NSButton!
    @IBOutlet weak var buttonReactor: NSButton!
    
    var globalPlay = AVAudioPlayer()
    var audioQ = AVAudioPlayer()
    var audioW = AVAudioPlayer()
    var audioE = AVAudioPlayer()
    var audioR = AVAudioPlayer()
    var audioA = AVAudioPlayer()
    var audioS = AVAudioPlayer()
    var audioD = AVAudioPlayer()
    var audioF = AVAudioPlayer()
    var audioZ = AVAudioPlayer()
    var audioX = AVAudioPlayer()
    var audioC = AVAudioPlayer()
    var audioV = AVAudioPlayer()
    var audioSpace = AVAudioPlayer()
    
    func getFile(fileName: String) -> NSURL {
        var fileURL = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource( fileName , ofType: "aif" )!)
        return fileURL!
    }
    
    func playFile(fileName: String) {
        globalPlay = AVAudioPlayer(contentsOfURL: getFile(fileName), error: nil)
        globalPlay.play()
    }
    
    override func viewWillAppear() {
        var button = view.window?.standardWindowButton(NSWindowButton.ZoomButton)
        button?.hidden = true
        
        //Window Background
        self.appBG.image = StyleKit.imageOfWindowFrame
        
        //Draw Buttons
        self.buttonQ.image = StyleKit.imageButton(baseButtonColor: StyleKit.redButtonColor, pressed: false)
        self.buttonQ.alternateImage = StyleKit.imageButton(baseButtonColor: StyleKit.redButtonColor, pressed: true)
        
        self.buttonW.image = StyleKit.imageButton(baseButtonColor: StyleKit.greenButtonColor, pressed: false)
        self.buttonW.alternateImage = StyleKit.imageButton(baseButtonColor: StyleKit.greenButtonColor, pressed: true)
        
        self.buttonE.image = StyleKit.imageButton(baseButtonColor: StyleKit.cyanButtonColor, pressed: false)
        self.buttonE.alternateImage = StyleKit.imageButton(baseButtonColor: StyleKit.cyanButtonColor, pressed: true)
        
        self.buttonR.image = StyleKit.imageButton(baseButtonColor: StyleKit.purpleButtonColor, pressed: false)
        self.buttonR.alternateImage = StyleKit.imageButton(baseButtonColor: StyleKit.purpleButtonColor, pressed: true)
        
        self.buttonA.image = StyleKit.imageButton(baseButtonColor: StyleKit.blueButtonColor, pressed: false)
        self.buttonA.alternateImage = StyleKit.imageButton(baseButtonColor: StyleKit.blueButtonColor, pressed: true)
        
        self.buttonS.image = StyleKit.imageButton(baseButtonColor: StyleKit.orangeButtonColor, pressed: false)
        self.buttonS.alternateImage = StyleKit.imageButton(baseButtonColor: StyleKit.orangeButtonColor, pressed: true)
        
        self.buttonD.image = StyleKit.imageButton(baseButtonColor: StyleKit.pinkButtonColor, pressed: false)
        self.buttonD.alternateImage = StyleKit.imageButton(baseButtonColor: StyleKit.pinkButtonColor, pressed: true)
        
        self.buttonF.image = StyleKit.imageButton(baseButtonColor: StyleKit.aquaButtonColor, pressed: false)
        self.buttonF.alternateImage = StyleKit.imageButton(baseButtonColor: StyleKit.aquaButtonColor, pressed: true)
        
        self.buttonZ.image = StyleKit.imageButton(baseButtonColor: StyleKit.blackButtonColor, pressed: false)
        self.buttonZ.alternateImage = StyleKit.imageButton(baseButtonColor: StyleKit.blackButtonColor, pressed: true)
        
        self.buttonX.image = StyleKit.imageButton(baseButtonColor: StyleKit.darkBlueButtonColor, pressed: false)
        self.buttonX.alternateImage = StyleKit.imageButton(baseButtonColor: StyleKit.darkBlueButtonColor, pressed: true)
        
        self.buttonC.image = StyleKit.imageButton(baseButtonColor: StyleKit.darkGreenButtonColor, pressed: false)
        self.buttonC.alternateImage = StyleKit.imageButton(baseButtonColor: StyleKit.darkGreenButtonColor, pressed: true)
        
        self.buttonV.image = StyleKit.imageButton(baseButtonColor: StyleKit.lightGrayButtonColor, pressed: false)
        self.buttonV.alternateImage = StyleKit.imageButton(baseButtonColor: StyleKit.lightGrayButtonColor, pressed: true)
        
        self.buttonReactor.image = StyleKit.imageReactor
        self.buttonReactor.alternateImage = StyleKit.imageReactor
        
        //Button Labels
        let parStyle = NSMutableParagraphStyle()
        parStyle.alignment = NSTextAlignment.CenterTextAlignment
        
        let buttonLabelAttributes = [
            NSForegroundColorAttributeName: NSColor.whiteColor(),
            NSFontAttributeName: NSFont(name:"Amatic SC Bold", size: 18)!,
            NSParagraphStyleAttributeName: parStyle
        ]
        
        self.buttonQ.attributedTitle = NSAttributedString(string:"PETER!",attributes:buttonLabelAttributes)
        self.buttonW.attributedTitle = NSAttributedString(string:"Profe",attributes:buttonLabelAttributes)
        self.buttonE.attributedTitle = NSAttributedString(string:"Delito",attributes:buttonLabelAttributes)
        self.buttonR.attributedTitle = NSAttributedString(string:"Espectacular",attributes:buttonLabelAttributes)
        
        self.buttonA.attributedTitle = NSAttributedString(string:"Fuera!",attributes:buttonLabelAttributes)
        self.buttonS.attributedTitle = NSAttributedString(string:"Somewhere",attributes:buttonLabelAttributes)
        self.buttonD.attributedTitle = NSAttributedString(string:"Best Cry",attributes:buttonLabelAttributes)
        self.buttonF.attributedTitle = NSAttributedString(string:"Patán",attributes:buttonLabelAttributes)
        
        self.buttonZ.attributedTitle = NSAttributedString(string:"Deal With It",attributes:buttonLabelAttributes)
        self.buttonX.attributedTitle = NSAttributedString(string:"Juaan!",attributes:buttonLabelAttributes)
        self.buttonC.attributedTitle = NSAttributedString(string:"Combo Breaker!",attributes:buttonLabelAttributes)
        self.buttonV.attributedTitle = NSAttributedString(string:"Cuack!",attributes:buttonLabelAttributes)
        
        self.buttonSpace.title = " "

    }
    
    //Button Actions
    @IBAction func buttonQ(sender: AnyObject) {
        audioQ = AVAudioPlayer(contentsOfURL: getFile("peter"), error: nil)
        audioQ.play()
    }
    @IBAction func buttonW(sender: AnyObject) {
        audioW = AVAudioPlayer(contentsOfURL: getFile("que-pedazo"), error: nil)
        audioW.play()
    }
    @IBAction func buttonE(sender: AnyObject) {
        audioE = AVAudioPlayer(contentsOfURL: getFile("delito"), error: nil)
        audioE.play()
    }
    @IBAction func buttonR(sender: AnyObject) {
        audioR = AVAudioPlayer(contentsOfURL: getFile("espectacular"), error: nil)
        audioR.play()

    }
    
    @IBAction func buttonA(sender: AnyObject) {
        audioA = AVAudioPlayer(contentsOfURL: getFile("fuera"), error: nil)
        audioA.play()
    }
    @IBAction func buttonS(sender: AnyObject) {
        audioS = AVAudioPlayer(contentsOfURL: getFile("somewhere"), error: nil)
        audioS.play()
    }
    @IBAction func buttonD(sender: AnyObject) {
        audioD = AVAudioPlayer(contentsOfURL: getFile("best-cry"), error: nil)
        audioD.play()
    }
    @IBAction func buttonF(sender: AnyObject) {
        audioF = AVAudioPlayer(contentsOfURL: getFile("patan"), error: nil)
        audioF.play()
    }
    
    @IBAction func buttonZ(sender: AnyObject) {
        audioZ = AVAudioPlayer(contentsOfURL: getFile("turn-down-for-what"), error: nil)
        audioZ.play()
    }
    @IBAction func buttonX(sender: AnyObject) {
        audioX = AVAudioPlayer(contentsOfURL: getFile("tan-helao-que-estai-juan"), error: nil)
        audioX.play()
    }
    @IBAction func buttonC(sender: AnyObject) {
        audioC = AVAudioPlayer(contentsOfURL: getFile("combo-breaker"), error: nil)
        audioC.play()
    }
    @IBAction func buttonV(sender: AnyObject) {
        audioV = AVAudioPlayer(contentsOfURL: getFile("cuack"), error: nil)
        audioV.play()
    }
    
    
    @IBAction func buttonSpacer(sender: AnyObject) {
        audioSpace = AVAudioPlayer(contentsOfURL: getFile("slomo-cuack"), error: nil)
        audioSpace.play()
    }
    @IBAction func buttonReactor(sender: AnyObject) {
        NSWorkspace.sharedWorkspace().openURL(NSURL(string:"http://www.reactor.cl")!)
    }
    
    /*
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override var representedObject: AnyObject? {
        didSet {
            // Update the view, if already loaded.
        }
    }
    */

}
