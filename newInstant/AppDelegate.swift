//
//  AppDelegate.swift
//  newInstant
//
//  Created by Guillermo Cádiz Pizarro on 24-06-15.
//  Copyright (c) 2015 Guillermo Cádiz Pizarro. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    var defaultWindow:NSWindow!
    
    func applicationDidFinishLaunching(aNotification: NSNotification) {
        // Insert code here to initialize your application
    }
    
    func applicationDidBecomeActive(notification: NSNotification) {
        defaultWindow = NSApplication.sharedApplication().windows.first as? NSWindow
        if ( defaultWindow.visible == false ) {
            defaultWindow.orderFront(self)
        }
    }
    
    func applicationShouldTerminateAfterLastWindowClosed(sender: NSApplication) -> Bool {
        return true
    }


    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application

    }
    
}
